
export const credentials = [
  { "emailOrUserName": "admin@verity.com", "password": "admin@123" },
]
export const URL = {

  base: "http://192.168.0.111/",
  serverApi: "http://192.168.0.111/api",

  base: "http://localhost:3030",
  serverApi: "http://localhost:3030/api",
}
export const mongoIds = {
  startId: "6447ad802f8c93a0ef9e460c",
  endId: "6447adb62f8c93a0efa03537"
}

export const api = {
  login: URL.serverApi + "/common/login",
  testUrls: [
    { // 0 // getting Organization/Facility list 
      url: URL.serverApi + "/admin/organization-facility-list",
      method: 'get',
      authenticationRequired: true,
    },
    { // 1 // getting role list
      url: URL.serverApi + "/admin/getAllRoles",
      method: 'get',
      authenticationRequired: true,
    },
    { // 2 // getting user list
      url: URL.serverApi + "/admin/getAllUsers",
      method: 'get',
      authenticationRequired: true,
    },
    { // 3 //  getting physician list
      url: URL.serverApi + "/admin/physician-list",
      method: 'get',
      authenticationRequired: true,
    },
    { // 4 // getting KPI list
      url: URL.serverApi + "/admin/getKpiData",
      method: 'get',
      authenticationRequired: true,
    },
    { // 5 // getting Masteres Data list 
      url: URL.serverApi + "/admin/masterData",
      method: 'get',
      authenticationRequired: true,
    },
    { // 6 // getting patient list
      url: URL.serverApi + "/patient/patients-list",
      method: 'get',
      authenticationRequired: true,
    },
    { // 7 // getting patient discipline's data
      url: URL.serverApi + "/patient/discipline",
      method: 'get',
      authenticationRequired: true,
    },
    { // 8 // getting episodes data byb patientIr
      url: URL.serverApi + "/patient/episodes-by-patientId",
      method: 'get',
      authenticationRequired: true,
    },
    { // 9 // getting financial Classes
      url: URL.serverApi + "/common/financial-class",
      method: 'get',
      authenticationRequired: true,
    },
    { // 10 // getting Facility's based on who is logged in
      url: URL.serverApi + "/common/facilities-by-user",
      method: 'get',
      authenticationRequired: true,
    },
    { // 11// getting lis tof physicians
      url: URL.serverApi + "/admin/physicians-by-name",
      method: 'get',
      authenticationRequired: true,
    },
    { // 12 // getting List of therapist
      url: URL.serverApi + "/common/therapist",
      method: 'get',
      authenticationRequired: true,
    },
    { // 13 // getting patient details on Vie page
      url: URL.serverApi + "/patient/patient-details",
      method: 'get',
      subRouted: true,
      authenticationRequired: true,
    },
    { // 14 // getting user's role details by role Id
      url: URL.serverApi + "/admin/getRole",
      method: 'get',
      subRouted: true,
      authenticationRequired: true,
    },
    { // 15 // getting insurance companies list
      url: URL.serverApi + "/patient/insurance-companies",
      method: 'get',
      subRouted: true,
      authenticationRequired: true,
    },
    { // 16 // getting list of diagnosis codes
      url: URL.serverApi + "/patient/diagnosis-codes",
      method: 'get',
      authenticationRequired: true,
    },
    { // 17 // getting list of approved insurance companies
      url: URL.serverApi + "/patient/insurance-companies-by-patientId",
      method: 'get',
      subRouted: true,
      authenticationRequired: true,
    },
    { // 18 // getting list of approved insurance companies
      url: URL.serverApi + "/common/notificationsCount",
      method: 'get',
      authenticationRequired: true,
    },
    { // 19 // getting list of approved insurance companies
      url: URL.serverApi + "/common/billed-units-by-user",
      method: 'get',
      authenticationRequired: true,
    },
    { // 20 // getting list of approved insurance companies
      url: URL.serverApi + "/patient/last-stored-patient-id",
      method: 'get',
      subRouted: true,
      authenticationRequired: true,
    },
    { // 21 // getting list of approved insurance companies
      url: URL.serverApi + "/patient/check-unique-field",
      method: 'get',
      authenticationRequired: true,
    },
    { // 22 // getting patient-timestamps
      url: URL.serverApi + "/patient/patients-timestamps",
      method: 'get',
      authenticationRequired: true,
    },
    { // 23 // getting patient-timestamps
      url: URL.serverApi + "/patient/patient-copy",
      method: 'patch',
      authenticationRequired: true,
    },
    { // 24 // getting patient-timestamps
      url: URL.serverApi + "/patient/patient-copy",
      method: 'delete',
      authenticationRequired: true,
    },
    { // 25 // getting patient-timestamps
      url: URL.serverApi + "/patient/authorization",
      method: 'put',
      withPatientData: true,
      authenticationRequired: true,
    },
    { // 26 // getting patient-timestamps
      url: URL.serverApi + "/patient/patient-by-id",
      method: 'get',
      subRouted: true,
      hasQuery: true,
      authenticationRequired: true,
    },
    { // 27 // getting patient-timestamps
      url: URL.serverApi + "/patient/authorization",
      method: 'delete',
      withPatientData: true,
      authenticationRequired: true,
    },
  ]
}