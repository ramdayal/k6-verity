import { URL } from "./config.js"
import { chooseRandomFromArray, getAuthHeader, getRandomNumberBetween, getUrlWithParams, objValToStr, randBool } from "./helpers.js"
const getPayload = (data = { api: {}, patientData: {}, creds: {} }) => {

  let headers = getAuthHeader(data.creds)

  // getting organization or faiclity lsit
  if ("/admin/organization-facility-list" == data.api.url && data.api.method == "get") {
    let params = {
      findQuery: { "deleteStatus": false, "type": chooseRandomFromArray(["organization", "facility"]) },
      skipCount: 0,
      sortCondition: { "createdAt": -1 }
    }
    if (randBool()) params.sortCondition[chooseRandomFromArray(["name", "parentOrganizationOrFacility", "mainPhone", "address.address1", "facilityCode"])] = chooseRandomFromArray([1, -1])
    params = objValToStr(params)
    return [getUrlWithParams(URL.serverApi + data.api.url, params), { headers }]
  }
  //getting lsit of roles
  else if ("/admin/getAllRoles" == data.api.url && data.api.method == "get") {
    let params = {
      findQuery: {
        "deleteStatus": false,
        "id": { "$ne": 1 },
      },
      sortCondition: { "roleOrder": -1, "createdAt": -1 }
    }
    if (randBool()) params.sortCondition["name"] = chooseRandomFromArray([1, -1])
    params = objValToStr(params)
    return [getUrlWithParams(URL.serverApi + data.api.url, params), { headers }]
  }

  // getting list of all users
  else if ("/admin/getAllUsers" == data.api.url && data.api.method == "get") {
    const params = {
      sortingKey: chooseRandomFromArray(['createdAt']),
      orderBy: chooseRandomFromArray([1, -1]),
      searchObject: JSON.stringify({
        "staffId": "",
        "firstName": "",
        "lastName": "",
        "facilityId": "",
        "organizationId": "",
        "roleId": ""
      }),
      skipCount: getRandomNumberBetween(0, 100),
      limit: 20,
    }
    return [getUrlWithParams(URL.serverApi + data.api.url, params), { headers }]
  }


  // getting list of all physicians
  else if ("/admin/physician-list" == data.api.url && data.api.method == "get") {
    let sortCondition = { "createdAt": -1 }
    if (randBool()) sortCondition[chooseRandomFromArray(["firstName", "contact.fax", "npi", "contact.telecom.contactNumber", "contact.email", "address.address1"])] = chooseRandomFromArray([1, -1])
    const params = {
      findQuery: '{"deleteStatus":false}',
      skipCount: getRandomNumberBetween(0, 100),
      sortCondition: JSON.stringify(sortCondition),
    }
    return [getUrlWithParams(URL.serverApi + data.api.url, params), { headers }]
  }

  // get all Kpi Data
  else if ("/admin/getKpiData" == data.api.url && data.api.method == "get") {

    let sortCondition = { "createdAt": -1 }
    if (randBool()) sortCondition[chooseRandomFromArray(["name", "spotLightLowRange", "spotLightHighRange", "description"])] = chooseRandomFromArray([1, -1])
    const params = {
      noOfRows: getRandomNumberBetween(0, 100),
      sortCondition: JSON.stringify(sortCondition),
      sorting: chooseRandomFromArray([true, false])
    }
    return [getUrlWithParams(URL.serverApi + data.api.url, params), { headers }]
  }

  // get list of masters data
  else if ("/admin/masterData" == data.api.url && data.api.method == "get") {
    const params = {
      code: "",
      skipCount: getRandomNumberBetween(0, 100),
      type: chooseRandomFromArray(["rehab", "medical", "cpt", "cap", "homeHealth", "pep"]),
      sortingkey: chooseRandomFromArray(["createdAt", "description", "code"]),
      sortingOrder: chooseRandomFromArray(["1", "-1"]),
    }
    return [getUrlWithParams(URL.serverApi + data.api.url, params), { headers }]
  }

  // getting patient List
  else if ("/patient/patients-list" == data.api.url && data.api.method == "get") {
    let findQuery = {
      "deleteStatus": false,
      "activeLinkedPatientId": null,
      "disciplineType": chooseRandomFromArray(["PT", "OT", "ST", "Dysphagia"])
    }
    let sortCondition = {
      "statusCode": 1,
      "createdAt": -1,
      "disciplineType": 1
    }
    if (randBool()) sortCondition = {
      therapy: findQuery.disciplineType,
      [chooseRandomFromArray(["patientFirstName", "admittedOn", "evalDate", "certExpDate", "lastVisitDate", "visitsTillProgressNote", "disciplineType", "facilityName", "statusCode"])]: chooseRandomFromArray([1, -1])
    }
    const params = {
      findQuery: JSON.stringify(findQuery),
      sortCondition: JSON.stringify(sortCondition),
      skipCount: getRandomNumberBetween(0, 100),
      fetchToken: new Date().getTime(), // can be anything // not used in any calculation in server side
      currentDate: new Date().toISOString().slice(0, 10),
    }
    return [getUrlWithParams(URL.serverApi + data.api.url, params), { headers }]
  }

  // getting patient's discipline Data
  else if ("/patient/discipline" == data.api.url && data.api.method == "get") {
    const params = {
      patientId: data.patientData._id,
      disciplineType: chooseRandomFromArray(data.patientData.discipline).type
    }
    return [getUrlWithParams(URL.serverApi + data.api.url, params), { headers }]
  }

  // getting patient's all episode data
  else if ("/patient/episodes-by-patientId" == data.api.url && data.api.method == "get") {
    let params = { patientId: getRandomNumberBetween(1103, 1202) }
    return [getUrlWithParams(URL.serverApi + data.api.url, params), { headers }]
  }

  // getting financial class data
  else if ("/common/financial-class" == data.api.url && data.api.method == "get") {
    return [URL.serverApi + data.api.url, { headers }]
  }

  // getting facilities list by user Id
  else if ("/common/facilities-by-user" == data.api.url && data.api.method == "get") {
    let params = { userId: (data.creds && data.creds.userId) ? data.creds.userId : "639a2f3a5fb3775429df3134" }
    return [getUrlWithParams(URL.serverApi + data.api.url, params), { headers }]
  }

  // getting list of physicians
  else if ("/admin/physicians-by-name" == data.api.url && data.api.method == "get") {
    let params = { userId: (data.creds && data.creds.userId) ? data.creds.userId : "639a2f3a5fb3775429df3134" }
    return [getUrlWithParams(URL.serverApi + data.api.url, params), { headers }]
  }

  // getting list of therapists
  else if ("/common/therapist" == data.api.url && data.api.method == "get") {
    const params = {
      facilityId: (data.creds && data.creds.facilityId && data.creds.facilityId.length && data.creds.facilityId[0]) ? data.creds.facilityId[0] : "6447a6a963ff8a8d4ddde057",
      getTherapistsAndAssistants: chooseRandomFromArray([true, false]),
    }
    return [getUrlWithParams(URL.serverApi + data.api.url, params), { headers }]
  }

  // getting details of patient
  else if ("/patient/patient-details" == data.api.url && data.api.method == "get") {
    const params = "/" + data.patientData._id // sample 6447ad822f8c93a0ef9e5775
    return [URL.serverApi + data.api.url + params, { headers }]
  }

  // getting role data based on role Id
  else if ("/admin/getRole" == data.api.url && data.api.method == "get") {
    const params = "/" + data.creds.roleId // sample 6447ad822f8c93a0ef9e5775
    return [URL.serverApi + data.api.url + params, { headers }]
  }

  // getting insurance companies list
  else if ("/patient/insurance-companies" == data.api.url && data.api.method == "get") {
    return [URL.serverApi + data.api.url, { headers }]
  }

  // getting list of diagnosis codes
  else if ("/patient/diagnosis-codes" == data.api.url && data.api.method == "get") {
    let params = { codeType: chooseRandomFromArray(["cpt", "medical", "rehab"]) }
    return [getUrlWithParams(URL.serverApi + data.api.url, params), { headers }]
  }

  // getting list of approved insurance companies
  else if ("/patient/insurance-companies-by-patientId" == data.api.url && data.api.method == "get") {
    return [URL.serverApi + data.api.url + "/" + data.patientData._id, { headers }]
  }

  // getting count of notification
  else if ("/common/notificationsCount" == data.api.url && data.api.method == "get") {
    let params = { userId: data.creds.userId }
    return [getUrlWithParams(URL.serverApi + data.api.url, params), { headers }]
  }

  // getting number of billed units
  else if ("/common/billed-units-by-user" == data.api.url && data.api.method == "get") {
    let params = {
      userId: (data.creds && data.creds.userId) ? data.creds.userId : "6447a68e1eb3f58f2f7748b7",
      logDate: new Date().toISOString().slice(0, 10)
    }
    return [getUrlWithParams(URL.serverApi + data.api.url, params), { headers }]
  }

  // getting last stored patient Id
  else if ("/patient/last-stored-patient-id" == data.api.url && data.api.method == "get") {
    return [URL.serverApi + data.api.url, { headers }]
  }

  // checking if give fields are unique or not
  else if ("/patient/check-unique-field" == data.api.url && data.api.method == "get") {
    const params = {
      field: chooseRandomFromArray(["socialSecurityNumber", "medicareHICno", "medicalRecordNumber"]),
      value: getRandomNumberBetween(1000000000, 9999999999),
      patientId: getRandomNumberBetween(2000, 134000),
      isGlobalSearch: chooseRandomFromArray([true, false]),
    }
    return [getUrlWithParams(URL.serverApi + data.api.url, params), { headers }]
  }

  // getting timestamps
  else if ("/patient/patients-timestamps" == data.api.url && data.api.method == "get") {
    return [URL.serverApi + data.api.url, { headers }]
  }

  // to make a copy of patient
  else if ("/patient/patient-copy" == data.api.url && data.api.method == "patch") {
    let payload = { id: data.patientData._id }
    return [URL.serverApi + data.api.url, JSON.stringify(payload), { headers }]
  }

  // to delete the copy of patient
  else if ("/patient/patient-copy" == data.api.url && data.api.method == "del") {
    let payload = { id: data.patientData._id }
    return [getUrlWithParams(URL.serverApi + data.api.url, payload), { headers }]
  }

  // to details of patient based on custom condition
  else if ("/patient/patient-by-id" == data.api.url && data.api.method == "get") {
    let payload = {
      find: JSON.stringify({
        "patientId": getRandomNumberBetween(1103, 1202),
        "insuranceInfo.insurance": { $gte: { $size: 1 } },
        "insuranceInfo.insurance.effectiveDate": { "$ne": null },
        "authorization": { $gte: { $size: 1 } },
        "authorization.startDate": { "$ne": null }
      })
    }
    return [getUrlWithParams(URL.serverApi + data.api.url, payload), { headers }]
  }

  // to update authorization data
  else if ("/patient/authorization" == data.api.url) {
    let authorizationData = chooseRandomFromArray(data.patientData.authorization)
    if (!authorizationData) return null

    // to update authorizarion
    if (data.api.method == 'put') {
      let insuranceData = chooseRandomFromArray(data.patientData.insuranceInfo.insurance)
      if (!insuranceData) {
        return null
      }
      authorizationData.startDate = insuranceData.effectiveDate
      authorizationData.endDate = insuranceData.terminationDate
      authorizationData.workerComplaintNumber = `${getRandomNumberBetween(1000, 99999)}`
      let payload = {
        "patientId": data.patientData._id,
        "externalId": data.patientData.patientId,
        "authorizationDetails": authorizationData
      }
      return [URL.serverApi + data.api.url, JSON.stringify(payload), { headers }]
    }

    // to delete authorization
    else if (data.api.method == 'del') {
      let params = {
        "patientId": data.patientData._id,
        "externalId": data.patientData.patientId,
        "authorizationId": authorizationData._id,
        "disciplineType": authorizationData.disciplineType,
      }
      return [getUrlWithParams(URL.serverApi + data.api.url, params), "{}", { headers }]
    }

    // to post new authorization
    else if (data.api.method == 'post') {
      let insuranceData = chooseRandomFromArray(data.patientData.insuranceInfo.insurance)
      if (!insuranceData) {
        return null
      }
      let newAuthorization = JSON.parse(JSON.stringify(authorizationData))
      newAuthorization.startDate = insuranceData.effectiveDate
      newAuthorization.endDate = insuranceData.terminationDate
      newAuthorization.workerComplaintNumber = `${getRandomNumberBetween(1000, 99999)}`
      newAuthorization.numberOfVisits = getRandomNumberBetween(10, 20)
      newAuthorization.visitsRemaining = newAuthorization.numberOfVisits
      let payload = {
        "patientId": data.patientData._id,
        "externalId": data.patientData.patientId,
        "authorizationDetails": newAuthorization
      }
      delete payload.authorizationDetails._id
      return [URL.serverApi + data.api.url, JSON.stringify(payload), { headers }]
    }
    return null
  }


  return null

}

export default getPayload