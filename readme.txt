./data-sets.js
  has credentails, urls, apis
  credentails array: it stores the array of username/email and password
    * you can add/remove credentials according to the URL of your server.
    * remember different server will have different credentials.
  baseURl is the client url
  serverApi is the backend proxy URL

  URL: stores the base URL of the server you want to test
  apis: It stores the routes you want to hit
    *note: every routes must send proper payload to get proper result


./helpers.js
  it stores many helper functions
  it has a variable names params
    the fields are url and for each url it returns a function that generates random payload for that payload.
    *you can edit the payload according to your requirement/ have it hard coded to always receive same response

./scripts.js
  this is executer file. You need to run this as K6 to start testing.
  export default is the main function that will be exectured for every virtual users
  options:
    it stores the number of Virtual users (VUS) as "target"
    duration is the amount in which the VUS will pe coded to sustain



to run the test use code "k6 run script.js"