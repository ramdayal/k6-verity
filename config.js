export const URL = {
  // base: "https://7d66-43-230-199-225.ngrok-free.app/",
  // serverApi: "https://7d66-43-230-199-225.ngrok-free.app/api",

  base: "http://localhost:3030",
  serverApi: "http://localhost:3030/api",

  // base: "http://44.204.249.51/", // this is the performance testing instance
  // serverApi: "http://44.204.249.51/api" // this is the performance testing instance
}

export const OPTIONS = {
  stages: [
    // { duration: '20s', target: 5 }, // target is the number of users // duration is for time
    { duration: '40s', target: 30 }, // target is the number of users // duration is for time
    { duration: '1m', target: 100 }, // target is the number of users // duration is for time
    { duration: '40s', target: 30 }, // target is the number of users // duration is for time
    // { duration: '20s', target: 5 }, // target is the number of users // duration is for time
  ],
}