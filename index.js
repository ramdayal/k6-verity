// import encoding from 'k6/encoding';
import http, { head } from 'k6/http';
import { check, sleep } from 'k6';

import { OPTIONS, URL } from './config.js';
import { getAuthHeader, getRandomApi, getRandomCredentials, getRandomNumberBetween, getUrlWithParams } from "./helpers.js"
import { commonApi } from "./apis.js"
import getPayload from "./payload.js"
export const options = OPTIONS;

const login = async function () {
  let credentials = getRandomCredentials()
  let requestBody = JSON.stringify({ credentials })
  let params = { headers: { 'Content-Type': 'application/json' } }

  let res = http.post(URL.serverApi + commonApi.login.url, requestBody, params);
  while (res.status == 429){
    console.log('status 429: Login', res.status)
    sleep(1)
    res = http.post(URL.serverApi + commonApi.login.url, requestBody, params);
  }
  check(res, { 'status was 200': (r) => r.status == 200 });
  sleep(5);
  return res.body ? JSON.parse(res.body) : null
}


export default async () => {

  let loginData = null
  if (__ENV.LOGIN_CREDS) {
    loginData = JSON.parse(__ENV.LOGIN_CREDS)
  } else {
    loginData = await login()
    sleep(1)
  }

  if (loginData) {
    if (!__ENV.LOGIN_CREDS) __ENV.LOGIN_CREDS = JSON.stringify(loginData)

    let selectedApi = null
    selectedApi = getRandomApi(0) // pass index if you want to hit same api repeatedly
    let patientData = null
    if (selectedApi.patientDataRequired) {
      if (!__ENV.PATIENT_DATA) {
        let query = { find: JSON.stringify({ patientId: getRandomNumberBetween(1208, 1213) }) }
        let url = getUrlWithParams(URL.serverApi + commonApi.patientData.url, query)
        let res = http.get(url, { headers: getAuthHeader(loginData) })
        while (res.status == 429){
          console.log('status 429: patient',)
          sleep(3)
          res = http.get(url, { headers: getAuthHeader(loginData) })
        }
        check(res, { 'status was 200': (r) => r.status == 200 })
        sleep(5)
        if (res && res.body && res.status == 200) {
          __ENV.PATIENT_DATA = res.body
          patientData = JSON.parse(res.body)
        } else { return }
      } else patientData = JSON.parse(__ENV.PATIENT_DATA)

    }

    let apiLoad = getPayload({ api: selectedApi, creds: loginData, patientData: patientData })
    if (!apiLoad) return
    // console.log('hitting', selectedApi.url, selectedApi.method)
    let res = http[selectedApi.method](...apiLoad)
    while (res.status == 429){ // too many request error, wait and hit again
      console.log('status 429: API')
      sleep(3)
      res = http[selectedApi.method](...apiLoad)
    }
    check(res, { 'status was 200': (r) => r.status == 200 });
    sleep(10)

    if (res && res.status != 200) {
      console.log('error with url:', res.status, selectedApi.method, selectedApi.url)
      // console.log(res)
    }


  }
}