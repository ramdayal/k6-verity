import { credentials, api, URL, mongoIds } from "./data-sets.js"
import { testApis } from "./apis.js"


export const getRandomCredentials = () => {
  return chooseRandomFromArray(credentials)
}

export const getRandomApi = (index) => {
  if (typeof (index) == 'number' && index >= 0 && index < testApis.length) return testApis[index]
  return chooseRandomFromArray(testApis)
}

export const getRandomMongoId = () => {
  let startId = mongoIds.startId
  let endId = mongoIds.endId
  const stringIndex = { 0: 0, 1: 1, 2: 2, 3: 3, 4: 4, 5: 5, 6: 6, 7: 7, 8: 8, 9: 9, a: 10, b: 11, c: 12, d: 13, e: 14, f: 15 }
  const stringSet = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"]
  let newValue = ""
  for (let i = 0; i < 24; i++) {
    let oIndex = stringIndex[startId[i]]
    let endIndex = stringIndex[endId[i]]
    let randomIndex = Math.floor(oIndex + (Math.random() * (endIndex + 1 - oIndex)))
    newValue += stringSet[randomIndex]
  }
  return newValue
}

export let chooseRandomFromArray = (array) => {
  let randomIndex = Math.floor(Math.random() * array.length)
  return array[randomIndex]
}
export const getAuthHeader = (loginObj) => {
  return {
    Authorization: JSON.stringify({ token: loginObj.token, userid: loginObj.userId }),
    'Content-Type': 'application/json'
  }
}
export const getRandomNumberBetween = (min, max) => {
  min = min ? min : 0
  max = max ? (max < min ? min : max) : 10
  return Math.floor(min + Math.random() * (max - min + 1))
}

export const objValToStr = (obj) => {
  let keys = Object.keys(obj)
  for (const key of keys) obj[key] = JSON.stringify(obj[key])
  return obj
}


export const getUrlWithParams = (url, params) => {
  let newUrl = url;
  let objectKeys = Object.keys(params)
  for (let i = 0; i < objectKeys.length; i++) {
    // newUrl += (i == 0 ? '?' : '&') + objectKeys[i] + '=' + (typeof (params[objectKeys[i]]) == 'object' ? JSON.parse(params[objectKeys[i]]) : params[objectKeys[i]])
    newUrl += (i == 0 ? '?' : '&') + objectKeys[i] + '=' + params[objectKeys[i]]
  }
  return newUrl
}

export const randBool = () => {
  return Math.floor(Math.random() * 2) ? true : false
}